//
//  DisplayUtils.swift
//  BleacherReport
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
var displayUtils = DisplayUtils()
class DisplayUtils{
    //global class for display utility functions
    
    
    func loadImageFromWeb(url:String,imageView:UIImageView){
        guard let actualURL = URL(string:  url) else { return }
        imageView.sd_setImage(with: actualURL, placeholderImage: nil)
    }
}

//
//  FlickPhoto.swift
//  FlickrStarterProject
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import Foundation
struct Photo: Codable {
    let id, owner, secret, server: String
    let farm: Int
    let title: String
    let ispublic, isfriend, isfamily: Int
    
    func getImageUrl () -> String {
        //based on Flickr API Docs
//        https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
        let url = "https://farm" + "\(farm)" + ".staticflickr.com/" + server + "/" + id + "_" + secret + ".jpg"
        return url
    }
}

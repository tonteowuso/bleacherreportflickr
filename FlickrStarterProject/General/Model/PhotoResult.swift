//
//  PhotoResult.swift
//  FlickrStarterProject
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import Foundation
struct PhotoResult:Codable{
    let photos: Photos
    let stat: String
}

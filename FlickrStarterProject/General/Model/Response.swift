//
//  Response.swift
//  FlickrStarterProject
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import Foundation
class Response{
    var success = false
    var code = 0
    var data:Data? = nil
    var message = ""
    
    
    //Parsing based on flickr api response
    
//    if successful
    //
    
    // if not
//    { "stat": "fail", "code": 100, "message": "Invalid API Key (Key not found)" }
    
    
    init(data:Data?) {
        self.data = data
        self.success = true
    }
    
    init(code:Int,message:String){
        self.success = false
        self.code = code
        self.message = message
    }
    
    
    
}

//
//  WebServiceUtils.swift
//  FlickrStarterProject
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import Foundation
struct URLS{
    static let API_KEY = "1508443e49213ff84d566777dc211f2a"
    static let BASE_URL = "https://www.flickr.com/services/rest"
}

enum WebServiceMethod:String{
    case search = "flickr.photos.search"
}


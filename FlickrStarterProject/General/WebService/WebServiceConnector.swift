//
//  WebServiceConnector.swift
//  FlickrStarterProject
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import Foundation

protocol WebServiceOutput {
    func webServiceCallComplete(method: WebServiceMethod, response: Response)
}

class WebServiceConnector{
    
     func makeWebServiceCall(method: WebServiceMethod, url: String, webServiceOutput: WebServiceOutput){
        guard let encodedURL = url.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed) else {return}
        guard let serviceUrl = URL(string: encodedURL) else {return}
            var request = URLRequest(url: serviceUrl)
            request.httpMethod = "GET"
            let session = URLSession.shared
            session.dataTask(with: request) { (data, response, error) in
                print(request)
                var apiResponse:Response? = nil
                //check if it got json data from the server
                if let data = data {
                      apiResponse = Response(data: data)
                    
                    do {
                        if let json = try JSONSerialization.jsonObject(with: data, options: []) as? NSObject{
                            print(json)
                            if let stat = json.value(forKey: "stat") as? String, let code = json.value(forKey: "code") as? Int, let message = json.value(forKey: "message") as? String{
                                if stat.lowercased() != "ok"{
                                     apiResponse = Response(code: code, message: message)
                                }
                            }
                           
                        }
                    } catch {
                        apiResponse =  Response(code: 0, message: error.localizedDescription)
                    }
                    
                 }
                else{
                    if let error = error{
                        apiResponse = Response(code: 0, message: error.localizedDescription)
                    }
                }
                
                DispatchQueue.main.async {
                    webServiceOutput.webServiceCallComplete(method: method, response: apiResponse ?? Response(code: 0, message: "Error connnecting to the server, please check your internet and try again"))
                }
                
                }.resume()
            }
    
    func search(text:String,page:String,size:String,webServiceOutput:WebServiceOutput){
        let url = URLS.BASE_URL + "?method=" + WebServiceMethod.search.rawValue + "&api_key=" + URLS.API_KEY + "&text=" + text + "&page=" + page + "&per_page=" + size + "&format=json&nojsoncallback=1"
        makeWebServiceCall(method: .search, url: url, webServiceOutput: webServiceOutput)
    }
    
}

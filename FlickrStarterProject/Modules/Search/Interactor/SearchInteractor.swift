//
//  SearchInteractor.swift
//  FlickrStarterProject
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import Foundation
class SearchInteractor:WebServiceOutput{
    var searchPresenter:SearchPresenter?
    var webServiceConnector:WebServiceConnector?
    
    func performSearch(text:String,page:Int,count:Int){
        self.webServiceConnector?.search(text: text, page: "\(page)",size: "\(count)", webServiceOutput: self)
    }
    
    func webServiceCallComplete(method: WebServiceMethod, response: Response){
        switch method {
        case .search:
            searchPresenter?.searchComplete(response: response)
        }
    }
}

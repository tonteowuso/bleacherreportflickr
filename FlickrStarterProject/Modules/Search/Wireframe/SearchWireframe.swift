//
//  SearchWireframe.swift
//  FlickrStarterProject
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import Foundation
import UIKit
class SearchWireframe{
    
    var searchPresenter:SearchPresenter?
    var searchNavigationController:UINavigationController?
    var mainSearchViewController:MainSearchViewController?
    var imageDetailsViewController:ImageDetailsViewController?
    
    let storyboardName = "Main"
    let mainSearchViewControllerIdentifier = "mainSearchView"
    let imageDetailsViewControllerIdentifier = "imageDetailsView"
    
    func returnInitialView() -> UINavigationController?{
        searchNavigationController = loadStoryboard().instantiateInitialViewController() as? UINavigationController
        if let mainSearchViewController = loadStoryboard().instantiateViewController(withIdentifier: mainSearchViewControllerIdentifier) as? MainSearchViewController{
            mainSearchViewController.searchPresenter = searchPresenter
            self.mainSearchViewController = mainSearchViewController
            searchNavigationController?.viewControllers = [mainSearchViewController]
        }
        return searchNavigationController
    }
    
    func returnDetailsView(_ photo:Photo) -> ImageDetailsViewController?{
        imageDetailsViewController = loadStoryboard().instantiateViewController(withIdentifier: imageDetailsViewControllerIdentifier) as? ImageDetailsViewController
        imageDetailsViewController?.photo = photo
        imageDetailsViewController?.searchPresenter = searchPresenter
        return imageDetailsViewController
    }
    func displayDetailsView(_ photo:Photo){
        guard let detailsViewController = returnDetailsView(photo) else {return}
        searchNavigationController?.pushViewController(detailsViewController, animated: true)
    }
    
    func loadStoryboard() -> UIStoryboard{
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard
    }
    
    func displayErrorMessage(title:String,message:String,completionHandler:(() -> Void)? = nil,okButtonText:String = "Ok" ){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
               alert.addAction(UIAlertAction(title: okButtonText, style: .default, handler: {(UIAlertAction)  in
                   if let handler = completionHandler{
                       handler()
                   }}
               ))
            self.searchNavigationController?.present(alert, animated: true)
       }
}

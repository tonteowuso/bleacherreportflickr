//
//  SearchPresenter.swift
//  FlickrStarterProject
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import Foundation
class SearchPresenter{
    var searchWireframe:SearchWireframe?
    var searchInteractor:SearchInteractor?
    
    
    func search(text:String,page:Int,count:Int){
        self.searchInteractor?.performSearch(text: text, page: page, count: count)
    }
    
    func searchComplete(response:Response){
        if response.success{
            if let result = response.data{
                let decoder = JSONDecoder()
                do {
                    let photos = try decoder.decode(PhotoResult.self, from: result)
                    self.searchWireframe?.mainSearchViewController?.searchResultsLoaded(photos: photos.photos.photo)
                } catch let error {
                    if searchWireframe?.mainSearchViewController?.view?.window != nil{
                           searchWireframe?.mainSearchViewController?.hideProgress()
                       }
                     print("Error parsing photos from JSON because: \(error)")
                    self.searchWireframe?.displayErrorMessage(title: "Error", message: "Error retrieving photos please try again")
                }
            }
           
        }
        else{
            if searchWireframe?.mainSearchViewController?.view?.window != nil{
                searchWireframe?.mainSearchViewController?.hideProgress()
            }
            self.searchWireframe?.displayErrorMessage(title: "Error", message: response.message)
        }
    }
    
    func showImageDetails(_ photo:Photo){
        self.searchWireframe?.displayDetailsView(photo)
    }
}

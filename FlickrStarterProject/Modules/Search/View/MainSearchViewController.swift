//
//  MainSearchViewController.swift
//  FlickrStarterProject
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import UIKit

class MainSearchViewController: UIViewController,UISearchBarDelegate,UITableViewDataSource,UITableViewDelegate{
   
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var countLabel: UILabel!
    
    @IBOutlet weak var instructionLabel: UILabel!
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    @IBOutlet var resultsTableView: UITableView!
    var searchPresenter:SearchPresenter?
    var searchBar:UISearchBar = UISearchBar()
    var photos:[Photo] = []
    var suggestions:[String] = []
    var suggesting = false
    var refreshControl: UIRefreshControl = UIRefreshControl()
    
    @IBOutlet weak var moreIndicator2: UIActivityIndicatorView!
    
     var noMorePhotos = false
    var size:Int = 25
    var searchText = ""
    var more = false
    var page = 1
     var userInfoStore:  UserDefaults = UserDefaults.standard
    let userInfoStoreKey = "searchSuggestions"
    override func viewDidLoad() {
        super.viewDidLoad()
         self.automaticallyAdjustsScrollViewInsets = false
        retrieveSearchSuggestions()
        configureSearchBar()
        configureTableView()
        searchBar.becomeFirstResponder()
        
        
        
    }
    
    func configureSearchBar(){
        searchBar = UISearchBar(frame: CGRect.init(x:0.0, y:0.0, width:view.frame.size.width, height:50.0))
        searchBar.delegate = self
        searchBar.showsSearchResultsButton = true
        searchBar.showsCancelButton = true
        searchBar.placeholder = "Search eg. Pokemon"
        self.navigationItem.titleView = searchBar
        searchBar.isHidden = false
    }
    
    func configureTableView(){
        resultsTableView.register(UINib(nibName: "ImageTableViewCell", bundle: nil), forCellReuseIdentifier: "imageCell")
        
        refreshControl.tintColor = UIColor.gray
        resultsTableView.addSubview(refreshControl)
         self.viewDidLayoutSubviews()
        resultsTableView.delegate = self
    }
    
    func searchResultsLoaded(photos:[Photo]){
        hideProgress()
        suggesting = false
        if more{
           
            if !photos.isEmpty{
                 page = page + 1
                self.photos = self.photos + photos
            }
            else{
                    noMorePhotos = true
            }
        }
        else{
            if !photos.isEmpty{
                page = 1
                self.photos = photos
            }
            else{
                self.searchPresenter?.searchWireframe?.displayErrorMessage(title: "Error", message: "Couldn't find any search results, please check the search term and try again")
            }
           
        }
        
        if !self.photos.isEmpty{
            countLabel.text = "Showing \(self.photos.count) items"
            countView.isHidden = false
        }
        else{
            countView.isHidden = true
        }
        
        resultsTableView.dataSource = self
        resultsTableView.reloadData()
    }
    
    func showSuggestions(show:Bool){
        self.suggesting = show
        if show{
            instructionLabel.isHidden = true
            countView.isHidden = true
            resultsTableView.isHidden = false
            resultsTableView.dataSource = self
            resultsTableView.reloadData()
        }
        else{
            searchFromAnotherViewController(text: self.searchText)
        }
    }
    
    func searchBarResultsListButtonClicked(_ searchBar: UISearchBar) {
        showSuggestions(show: !suggesting)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
         suggesting = false
        instructionLabel.isHidden = true
        searchBar.searchTextField.resignFirstResponder()
        let searchText = searchBar.searchTextField.text!
        if !searchText.isEmpty{
            self.searchText = searchText
            self.search(text: searchText)
            if !self.suggestions.contains(searchText){
                suggestions.append(searchText)
            }
            storeSearchSuggestions()
        }
        
    }
    
    func search(text:String){
        suggesting = false
        page = 1
        showProgressForSearch()
        more = false
        self.searchPresenter?.search(text: text,page: 1,count: size)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        showSuggestions(show: false)
         searchBar.searchTextField.resignFirstResponder()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            if suggesting{
                return suggestions.count
            }
            return self.photos.count
            
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "imageCell") as? ImageTableViewCell{
            if !suggesting{
                return cell
            }
        }
        return UITableViewCell()
       }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let index = indexPath.row
        if !suggesting{
            let photo = self.photos[index]
            if let cell = cell as? ImageTableViewCell {
                cell.titleLabel.text = photo.title
                displayUtils.loadImageFromWeb(url: photo.getImageUrl(), imageView: cell.thumbnailView)
            }
            if index == photos.count - 1
            {
                loadMoreResults()
            }
        }
        else{
            cell.textLabel?.text = suggestions[index]
        }
        
    }
    
    func loadMoreResults(){
        if !noMorePhotos {
            more = true
            moreIndicator2.startAnimating()
//            let page = (photos.count / size) + 1
            self.searchPresenter?.search(text: self.searchText,page: page + 1,count: size)
            
        }
    }
    
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if !suggesting{
            if refreshControl.isRefreshing{
               more = false
               page = 1
               noMorePhotos = false
               search(text: searchText)
           }
        }
       
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if suggesting{
                return 50
        }
        return 107
    }
    
    
    func showProgressForSearch(){
         self.activityIndicator.isHidden = false
        self.resultsTableView.isHidden = true
        countView.isHidden = true
    }
    
    func hideProgress(){
        if refreshControl.isRefreshing{
            refreshControl.endRefreshing()
        }
        self.resultsTableView.isHidden = false
        self.activityIndicator.isHidden = true
        moreIndicator2.stopAnimating()
        
    }

    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        if !suggesting{
            let photo = self.photos[indexPath.row]
            self.searchPresenter?.showImageDetails(photo)
        }
        else{
            self.searchFromAnotherViewController(text:self.suggestions[indexPath.row])
            
        }
       
        return nil
    }
    
    func searchFromAnotherViewController(text:String){
        searchBar.searchTextField.text = text
        searchBarSearchButtonClicked(searchBar)
    }
    
    
    // wanted to use core data but run out of time
    //TO-DO change this to core data
    func storeSearchSuggestions(){
        let userInfoData = NSKeyedArchiver.archivedData(withRootObject: self.suggestions)
        userInfoStore.removeObject(forKey: userInfoStoreKey)
        userInfoStore.set(userInfoData, forKey: userInfoStoreKey)
    }
    func retrieveSearchSuggestions(){
        if let userData = userInfoStore.object(forKey: userInfoStoreKey) as? NSData{
            if let userInfo = NSKeyedUnarchiver.unarchiveObject(with: userData as Data){
                if let search = userInfo as? [String]{
                    if !search.isEmpty{
                        self.suggestions = search
                    }
                }
            }
        }
    }
    
  
    
   

}

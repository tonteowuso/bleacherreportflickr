//
//  ImageDetailsViewController.swift
//  BleacherReport
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import UIKit

class ImageDetailsViewController: UIViewController,UISearchBarDelegate {
    var photo:Photo?
    
    @IBOutlet weak var fullImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    var searchBar:UISearchBar = UISearchBar()
    var searchPresenter:SearchPresenter?
    

    override func viewDidLoad() {
        super.viewDidLoad()
        if let photo = self.photo{
            displayUtils.loadImageFromWeb(url: photo.getImageUrl(), imageView: fullImageView)
            titleLabel.text = photo.title
        }
        configureSearchBar()
        // Do any additional setup after loading the view.
    }
    
    func configureSearchBar(){
        searchBar = UISearchBar(frame: CGRect.init(x:0.0, y:0.0, width:view.frame.size.width, height:50.0))
        searchBar.delegate = self
        searchBar.showsSearchResultsButton = false
        searchBar.showsCancelButton = true
        searchBar.placeholder = "Search eg. Pokemon"
        self.navigationItem.titleView = searchBar
        searchBar.isHidden = false
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar){
          
           searchBar.searchTextField.resignFirstResponder()
           let searchText = searchBar.searchTextField.text!
           if !searchText.isEmpty{
            self.navigationController?.popViewController(animated: true)
            searchPresenter?.searchWireframe?.mainSearchViewController?.searchFromAnotherViewController(text: searchText)
           }
           
       }

}

//
//  AppDependencies.swift
//  FlickrStarterProject
//
//  Created by Tonte on 05/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import Foundation
class AppDependencies{
    var webServiceConnector = WebServiceConnector()
    var searchWireframe = SearchWireframe()
    
    init() {
        injectDependencies()
    }
    
    func injectDependencies(){
        let searchPresenter = SearchPresenter()
        let searchInteractor = SearchInteractor()
        searchInteractor.webServiceConnector = webServiceConnector
        searchPresenter.searchInteractor = searchInteractor
        searchPresenter.searchWireframe = searchWireframe
        searchInteractor.searchPresenter = searchPresenter
        searchWireframe.searchPresenter = searchPresenter
    }
    
}

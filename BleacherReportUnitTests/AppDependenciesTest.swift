//
//  AppDependenciesTest.swift
//  BleacherReportTests
//
//  Created by Tonte on 06/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import XCTest
@testable import BleacherReport

class AppDependenciesTest: XCTestCase {
    let appDependencies = AppDependencies()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testDependenciesNotNil(){
        XCTAssertNotNil(appDependencies.searchWireframe)
           XCTAssertNotNil(appDependencies.webServiceConnector)
       }

    func testInjectDependencies(){
        XCTAssertNotNil(appDependencies.searchWireframe.searchPresenter)
        XCTAssertNotNil(appDependencies.searchWireframe.searchPresenter?.searchWireframe)
        XCTAssertNotNil(appDependencies.searchWireframe.searchPresenter?.searchInteractor)
        XCTAssertNotNil(appDependencies.searchWireframe.searchPresenter?.searchInteractor?.searchPresenter)
        XCTAssertNotNil(appDependencies.searchWireframe.searchPresenter?.searchInteractor?.webServiceConnector)
        
    }
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }


}

//
//  ImageDetailsViewControllerTest.swift
//  BleacherReportUnitTests
//
//  Created by Tonte on 06/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import XCTest
@testable import BleacherReport
class ImageDetailsViewControllerTest: XCTestCase {
    var window = UIWindow()
   var imageDetailsViewController = ImageDetailsViewController()

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let mockDisplayUtils = MockDisplayUtils()
               displayUtils = mockDisplayUtils
       
        
    }
    
    func testViewDidLoad(){
        setupTestViewController()
        XCTAssert(imageDetailsViewController.titleLabel.text == "title")
        XCTAssert(imageDetailsViewController.searchBar.placeholder == "Search eg. Pokemon")
    }
    
    func testSearchBarSearchButtonClicked(){
         setupTestViewController()
        imageDetailsViewController.searchBarSearchButtonClicked(imageDetailsViewController.searchBar)
    }

    func setupTestViewController(){
        let storyboardName = "Main"
       
             let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        imageDetailsViewController = storyboard.instantiateViewController(withIdentifier: "imageDetailsView") as? ImageDetailsViewController ?? ImageDetailsViewController()
         imageDetailsViewController.photo = Photo(id: "12", owner: "12", secret: "342", server: "1331", farm: 0, title: "title", ispublic: 1, isfriend: 1, isfamily: 1)
        window = UIWindow()
        window.addSubview(imageDetailsViewController.view)
        let _ = imageDetailsViewController.view
    }

    class MockDisplayUtils:DisplayUtils{
        override func loadImageFromWeb(url: String, imageView: UIImageView) {
            XCTAssert(true)
        }
    }

}

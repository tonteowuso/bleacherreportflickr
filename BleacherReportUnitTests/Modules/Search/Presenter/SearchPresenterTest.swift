//
//  SearchPresenterTest.swift
//  BleacherReportUnitTests
//
//  Created by Tonte on 06/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import XCTest
@testable import BleacherReport
class SearchPresenterTest: XCTestCase {
 var searchPresenter = SearchPresenter()
    override func setUp() {
        searchPresenter = SearchPresenter()
        searchPresenter.searchInteractor = MockSearchInteractor()
        searchPresenter.searchWireframe = MockSearchWireframe()
        searchPresenter.searchInteractor?.searchPresenter = searchPresenter
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testSearch(){
        searchPresenter.search(text: "sampleText", page: 1, count: 20)
    }
    
    func testSuccesfulSearch(){
        let data:[String:Any] = ["photos":["page": 1, "pages": 2, "perpage": 1, "total": "2", "photo": ["id": "12", "owner": "12", "secret": "342", "server": "1331", "farm": 0, "title": "title", "ispublic": 1, "isfriend": 1, "isfamily": 1]],"stat":"ok"]
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                   let response = Response(data: jsonData)
                   searchPresenter.searchComplete(response: response)
        } catch  {
            XCTAssert(false)
        }
       
    }
    func testSuccesfulSearchWithNoImagesReturned(){
        let data:[String:Any] = ["stat":"ok"]
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: data, options: .prettyPrinted)
                   let response = Response(data: jsonData)
                   searchPresenter.searchComplete(response: response)
        } catch  {
            XCTAssert(false)
        }
    }
    
    func testFailedSearch(){
        searchPresenter.searchComplete(response: Response(code: 0, message: "Error"))
    }
    

    
    class MockSearchInteractor:SearchInteractor{
        override func performSearch(text: String, page: Int, count: Int) {
//            just checking if it calls the function
            XCTAssert(text == "sampleText")
            XCTAssert(page == 1)
             XCTAssert(count == 20)
            
            
        }
    }
    
    func testShowImageDetails(){
        
        searchPresenter.showImageDetails(Photo(id: "12", owner: "12", secret: "342", server: "1331", farm: 0, title: "title", ispublic: 1, isfriend: 1, isfamily: 1))
        XCTAssertNotNil(searchPresenter.searchWireframe?.imageDetailsViewController)
        
    }
    
    class MockSearchWireframe:SearchWireframe{
        override func displayErrorMessage(title: String, message: String, completionHandler: (() -> Void)? = nil, okButtonText: String = "Ok") {
            XCTAssert(true)
        }
    }

    
    
    
    

}

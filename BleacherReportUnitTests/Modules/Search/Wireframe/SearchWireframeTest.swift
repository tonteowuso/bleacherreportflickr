//
//  SearchWireframeTest.swift
//  BleacherReportUnitTests
//
//  Created by Tonte on 06/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import XCTest
@testable import BleacherReport

class SearchWireframeTest: XCTestCase {
    var searchWireframe = SearchWireframe()
       var window:UIWindow = UIWindow()
    override func setUp() {
       searchWireframe = SearchWireframe()
        searchWireframe.searchPresenter = SearchPresenter()
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

   func testReturnInitialScreen(){
       let _ = searchWireframe.returnInitialView()
       XCTAssertNotNil(searchWireframe.searchNavigationController)
       XCTAssertNotNil(searchWireframe.mainSearchViewController)
       XCTAssertNotNil(searchWireframe.mainSearchViewController?.searchPresenter)
       XCTAssert((searchWireframe.searchNavigationController?.viewControllers.contains(searchWireframe.mainSearchViewController!))!)
       
   }
    
    func testReturnDetailsView(){
        let _ = searchWireframe.returnInitialView()
        let _ = searchWireframe.returnDetailsView(Photo(id: "12", owner: "12", secret: "342", server: "1331", farm: 0, title: "title", ispublic: 1, isfriend: 1, isfamily: 1))
        XCTAssertNotNil(searchWireframe.searchNavigationController)
        XCTAssertNotNil(searchWireframe.imageDetailsViewController)
        XCTAssertNotNil(searchWireframe.imageDetailsViewController?.searchPresenter)
        XCTAssertNotNil(searchWireframe.imageDetailsViewController?.photo)
        
    }
    
    func testDisplayDetailsView(){
        let _ = searchWireframe.returnInitialView()
               window = UIWindow()
               searchWireframe.displayDetailsView(Photo(id: "12", owner: "12", secret: "342", server: "1331", farm: 0, title: "title", ispublic: 1, isfriend: 1, isfamily: 1))
               window.addSubview(searchWireframe.imageDetailsViewController!.view)
               XCTAssert(searchWireframe.imageDetailsViewController!.isViewLoaded)
    }
    
    func testLoadStoryboard(){ XCTAssertNotNil(searchWireframe.loadStoryboard)
    }
    
    func testDisplayErrorMessage(){
        let _ = searchWireframe.returnInitialView()
        window = UIWindow()
       searchWireframe.displayErrorMessage(title: "Error", message: "test")
        searchWireframe.displayErrorMessage(title: "Error", message: "test",completionHandler:{ XCTAssert(true)})
    }
    
    

}

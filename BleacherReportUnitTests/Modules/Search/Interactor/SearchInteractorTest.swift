//
//  SearchInteractorTest.swift
//  BleacherReportUnitTests
//
//  Created by Tonte on 06/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import XCTest
@testable import BleacherReport

class SearchInteractorTest: XCTestCase {
    var searchInteractor = SearchInteractor()

    override func setUp() {
        searchInteractor = SearchInteractor()
        searchInteractor.searchPresenter = MockSearchPresenter()
        searchInteractor.webServiceConnector = MockWebServiceConnector()
        searchInteractor.searchPresenter?.searchInteractor = searchInteractor
    }

    func testPerformSearch(){
        searchInteractor.performSearch(text: "search", page: 0, count: 0)
    }
    func testWebServiceCallComplete(){
        searchInteractor.webServiceCallComplete(method: .search, response: Response(data: nil))
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    class MockSearchPresenter:SearchPresenter{
        override func searchComplete(response: Response) {
            XCTAssert(true)
        }
    }
    
    class MockWebServiceConnector:WebServiceConnector{
        override func search(text: String, page: String, size: String, webServiceOutput: WebServiceOutput) {
            XCTAssert(true)
        }
    }


}

//
//  WebServiceConnectorTest.swift
//  BleacherReportUnitTests
//
//  Created by Tonte on 06/02/2020.
//  Copyright © 2020 Tonte. All rights reserved.
//

import XCTest
@testable import BleacherReport

class WebServiceConnectorTest: XCTestCase {
    var webServiceConnector = WebServiceConnector()
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    func testSearch(){
        webServiceConnector.search(text: "test", page: "1", size: "1", webServiceOutput: SearchInteractor())
        
    }
   
}
